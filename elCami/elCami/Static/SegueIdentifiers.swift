//
//  SegueIdentifiers.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 05/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation

struct SegueIdentifiers {
    static let ShowTabViewIdentifier: String = "showTabView"
    static let ShowStateIdentifier: String = "showStateView"
    static let ShowCityIdentifier: String = "showCityView"
    static let ShowDistrictIdentifier: String = "showDistrictView"
}
