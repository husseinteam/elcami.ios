//
//  PredefinedUrls.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 06/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation

struct PredefinedUrls {
    
    static let PrayerUrl: String = "http://www.diyanet.gov.tr/tr/namazvakitleri"
    static let AyatHadithUrl: String = "http://www.ayethadis.com"
    
    static let FillStateRoute: String = "http://www.diyanet.gov.tr/PrayerTime/FillState?countryCode="
    static let FillCityRoute: String = "http://www.diyanet.gov.tr/PrayerTime/FillCity?itemId="
    static let PrayerTimesSetRoute: String = "http://www.diyanet.gov.tr/PrayerTime/PrayerTimesSet"

}
