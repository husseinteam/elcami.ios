//
//  DistrictsCollectionViewController.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 15/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import UIKit

fileprivate let reuseIdentifier = "Cell"

class DistrictsCollectionViewController: UICollectionViewController, DataProviderDelegate {
    typealias Element = CoreCity
    typealias TOwner = CoreState
    
    // MARK: - Outlets
    @IBOutlet weak var ActivityView: UIActivityIndicatorView!
    
    // MARK: - Declarations
    var owner: CoreState? = nil
    var data: [CoreCity]?
    var filtered: [CoreCity]?
    fileprivate let itemsPerRow = 3
    fileprivate let sectionInsets = UIEdgeInsets(top: 3.0, left: 3.0, bottom: 3.0, right: 3.0)
    
    // MARK: - Lifecycle Handlers
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.register(UINib(nibName: "DistrictViewCell", bundle: nil),
                                      forCellWithReuseIdentifier: reuseIdentifier)
        
        self.configureCollectionView()
        self.feedCollection()
        //self.enableCustomMenu()
    }
    
    // MARK: - Private Functions
    private func configureCollectionView() {
        self.collectionView?.backgroundView?.frame = CGRect.zero
        self.collectionView?.backgroundColor = UIColor.white
    }
    
    private func feedCollection() {
        if let owner = self.owner {
            City.seed(
                byState: owner,
                collect: { results in
                    self.data = results
                    self.filtered = results
                    self.collectionView?.reloadData()
                    self.ActivityView.stopAnimating()
            }) { (fpe, type, err) in
                self.ActivityView.stopAnimating()
            }
        }
    }
    
    private func enableCustomMenu() {
        let add = UIMenuItem()
        add.title = "Ekle"
        UIMenuController.shared.menuItems = [add]
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.filtered?.count ?? 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        if let districtCell = cell as? DistrictViewCell {
            districtCell.configureSwitch((controller: self, at: indexPath))
            if let coreCity = self.filtered?[indexPath.item] {
                districtCell.DistrictLabel.text = coreCity.name!
            }
            return districtCell
        } else {
            cell.backgroundColor = UIColor.red
            return cell
        }
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return NSStringFromSelector(action) == "Ekle"
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        if NSStringFromSelector(action) == "Ekle" {

        }
    }

}

extension DistrictsCollectionViewController : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * CGFloat(itemsPerRow + 1)
        let availableWidth = view.frame.width - 2.0*paddingSpace
        let widthPerItem = availableWidth / CGFloat(itemsPerRow)

        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    //2
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}
