//
//  ZhikirViewController.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 11/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import UIKit

class ZhikirViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var ZhikirTargetView: UIView!
    @IBOutlet weak var CurrentZhikirLabel: UILabel!
    @IBOutlet weak var ZhikirCounterLabel: UILabel!
    @IBOutlet weak var ZhikirTitleLabel: UILabel!
    
    // MARK: - Variables
    private var zhikirData = [Zhikir]()
    private var totalZhikirCount = 1
    private var progress: Int = 0
    private var index: Int = 0
    
    // MARK: - Lifecycle Handlers
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addZhikirGesture()
        self.prepareZhikirData()
        self.prepareSpinner()
    }
    
    // MARK: - Private Functions
    @objc private func makeNextZhikir() {
        guard self.index < self.zhikirData.count - 1 else {
            self.resetCounter()
            return
        }
        self.progress += 1
        let till = self.zhikirData[0...self.index].reduce(0) { c, z in c + z.Count }
        if till == self.progress {
            self.index = self.index + 1
        }
        let currentCount = self.progress - (self.zhikirData[0..<self.index].reduce(0) { c, z in c + z.Count }) + 1
        self.animateZhikirCount(count: currentCount)
        self.CurrentZhikirLabel.text = self.zhikirData[self.index].Transcript
        self.ZhikirTitleLabel.text =
            "\(self.zhikirData[self.index].Count - currentCount + 1) \(self.zhikirData[self.index].Title)"
        self.ZhikirTitleLabel.animateBubble(alphaBegin: 1.0, alphaEnd: 1.0, upperScale: 1.02)
        let spinner = SwiftSpinner.show(
            outerProgress: Double(self.progress)/Double(self.totalZhikirCount),
            innerProgress: Double(currentCount)/Double(self.zhikirData[self.index].Count))
        spinner.innerColor = UIColor.green
        spinner.outerColor = UIColor(hash: "#377935")
        spinner.titleLabel.textColor = UIColor(hash: "#008080")
    }
    private func animateZhikirCount(count: Int) {
        self.ZhikirCounterLabel.text = "\(count)"
        self.ZhikirCounterLabel.layer.zPosition = 99
        self.ZhikirCounterLabel.animateBubble(alphaBegin: 0.5, alphaEnd: 0.89, upperScale: 1.05)
    }
    @objc private func resetCounter() {
        self.totalZhikirCount = self.zhikirData.reduce(0) { c, z in
            return c + z.Count }
        self.progress = 0
        self.index = 0
        self.ZhikirCounterLabel.font = self.ZhikirCounterLabel.font.withSize(77.0)
        self.ZhikirCounterLabel.transform = CATransform3DGetAffineTransform(CATransform3DIdentity)
        self.animateZhikirCount(count: 1)
        self.CurrentZhikirLabel.text = self.zhikirData[0].Transcript
        self.ZhikirTitleLabel.text =
            "\(self.zhikirData[0].Count) \(self.zhikirData[0].Title)"
        SwiftSpinner.hide()
        SwiftSpinner.show(
            outerProgress: 0.0,
            innerProgress: 0.0)
    }
    
    // MARK: - Preparation Functions
    func addZhikirGesture() {
        self.ZhikirTargetView.isUserInteractionEnabled = true
        let zhikirTap = UITapGestureRecognizer(target: self,
                                               action: #selector(self.makeNextZhikir))
        self.ZhikirTargetView.addGestureRecognizer(zhikirTap)
        
        let resetTap = UILongPressGestureRecognizer(target: self,
                                               action: #selector(self.resetCounter))
        self.ZhikirTargetView.addGestureRecognizer(resetTap)
        
        self.ZhikirTargetView.layer.borderColor = UIColor(hash: "#96010F").cgColor
        self.ZhikirTargetView.layer.addBorder(edge: .all, color: UIColor.green, thickness: 1.0, padding: 0.0)
    }
    func prepareZhikirData() {
        self.zhikirData = [
            Zhikir("BİSMİLLAHİRRAHMANİRRAHİM"),
            Zhikir("Kelime-i Şehâdet", transcript: "Eşhedu en lâ ilâhe illallâh ve eşhedü enne Muhammeden Abdühü ve Resülühü", count: 3),
            Zhikir("Estâğfirullah", count: 70),
            Zhikir("Fatiha-ı Şerife", transcript: "Bismillahirrahmânirrahîm.. Elhamdü lillâhi rabbil'alemin. Errahmânir'rahim. Mâliki yevmiddin. İyyâke na'budü ve iyyâke neste'în. İhdinessırâtel müstakîm. Sırâtellezine en'amte aleyhim ğayrilmağdûbi aleyhim ve leddâllîn. Amîn"),
            Zhikir("Amen er resulü", transcript: "Bismillahirrahmânirrahîm.. Amenerrasulü bima ünzile ileyhi mirrabbihi vel mü’minun, küllün amene billahi vemelaiketihi ve kütübihi ve rusülih, la nüferriku beyne ehadin min rusülih, ve kalu semi’na ve ata’na gufraneke rabbena ve ileykelmesir. La yükellifullahü nefsenilla vüs’aha, leha ma kesebet ve aleyha mektesebet, rabbena latüahızna innesiyna ev ahta’na, rabbena vela tahmil aleyna ısran kema hameltehü alelleziyne min gablina, rabbena vela tühammilna, mala takatelena bih, va’fü anna, vağfirlena, verhamna, ente mevlana fensurna alel gavmil kafiriyn. Amîn"),
            Zhikir("Inşirah", transcript: "Bismillahirrahmânirrahîm. Elem neşrah leke sadrek Ve vada'na 'anke vizreke Elleziy enkada zahreke Ve refa'na leke zikreke Feinne me'al'usri yüsren İnne me'al'usri yüsren Feiza ferağte fensab Ve ila rabbike ferğab. Allâh-ü Ekber", count: 7),
            Zhikir("İhlas-ı Şerif", transcript: "Bismillahirrahmânirrahîm.. Kul hüvellâhü ehad. Allâhüssamed. Lem yelid ve lem yûled. Ve lem yekün lehû küfüven ehad. Allâh-ü Ekber", count: 11),
            Zhikir("Felak", transcript: "Bismillahirrahmânirrahîm.. Kul e'ûzü birabbil felak. Min şerri mâ halak. Ve min şerri ğasikın izâ vekab. Ve min şerrinneffâsâti fil'ukad. Ve min şerri hâsidin izâ hased. Allâh-ü Ekber"),
            Zhikir("Nas", transcript: "Bismillahirrahmânirrahîm.. Kul e'ûzü birabbinnâs. Melikinnâs. İlâhinnâs. Min şerrilvesvâsilhannâs. Ellezî yüvesvisü fî sudûrinnâsi. Minelcinneti vennâs. Allâh-ü Ekber"),
            Zhikir("Kelime-i Tevhid", transcript: "Lâ İlâhe İllallah", count: 9),
            Zhikir("Kelime-i Tevhid", transcript: "Lâ İlâhe İllallah Muhammedûn Resulullah"),
            Zhikir("Salavat-ı Şerife", transcript: "Allâhümme salli âla Muhammedin ve âla Âli Muhammedin ve sellim", count: 10),
            Zhikir("BAĞIŞLAMA", transcript: "Bu okuduklarımızdan hâsıl olan sevabı bilhassa Efendimizin (Sallallahü Aleyhi ve Sellem) Mübarek Rûh-u Şerîfine, Enbiya ve Evliyaların da rûhlarına, hassaten Şâh-ı Nakşibendi Hz.lerinin Rûh-u Şerîfine, Mevlânâ Şeyh Abdullah el Fâizi Dağıstanî Hz.lerinin Rûh-u Şerîfine, Mevlânâ Sultan-u Evliya Şeyh Muhammed Nazım El Hakkani Hz.lerinin Ruh-u Şerîfine hediyye eyledim. Ya Rabbi sen vâsıl eyle. Âmin..."),
            Zhikir("Fatiha-ı Şerife", transcript: "Bismillahirrahmânirrahîm.. Elhamdü lillâhi rabbil'alemin. Errahmânir'rahim. Mâliki yevmiddin. İyyâke na'budü ve iyyâke neste'în. İhdinessırâtel müstakîm. Sırâtellezine en'amte aleyhim ğayrilmağdûbi aleyhim ve leddâllîn. Amîn"),
            Zhikir("Lafz-ı Celil", transcript: "Allah", count: 1500),
            Zhikir("Salavat-ı Şerife", transcript: "Allâhümme salli âla Muhammedin ve âla Âli Muhammedin ve sellim", count: 300),
            Zhikir("İhlas-ı Şerif", transcript: "Bismillahirrahmânirrahîm.. Kul hüvellâhü ehad. Allâhüssamed. Lem yelid ve lem yûled. Ve lem yekün lehû küfüven ehad. Allâh-ü Ekber", count: 100),
            Zhikir("Salavat-ı Şerife", transcript: "Allâhümme salli âla Muhammedin ve âla Âli Muhammedin ve sellim", count: 100),
            Zhikir("BAĞIŞLAMA", transcript: "Bu okuduklarımızdan hâsıl olan sevabı bilhassa Efendimizin (Sallallahü Aleyhi ve Sellem) Mübarek Rûh-u Şerîfine, Enbiya ve Evliyaların da rûhlarına, hassaten Şâh-ı Nakşibendi Hz.lerinin Rûh-u Şerîfine, Mevlânâ Şeyh Abdullah el Fâizi Dağıstanî Hz.lerinin Rûh-u Şerîfine, Mevlânâ Sultan-u Evliya Şeyh Muhammed Nazım El Hakkani Hz.lerinin Ruh-u Şerîfine hediyye eyledim. Ya Rabbi sen vâsıl eyle. Âmin..."),
            Zhikir("Fatiha-ı Şerife", transcript: "Bismillahirrahmânirrahîm.. Elhamdü lillâhi rabbil'alemin. Errahmânir'rahim. Mâliki yevmiddin. İyyâke na'budü ve iyyâke neste'în. İhdinessırâtel müstakîm. Sırâtellezine en'amte aleyhim ğayrilmağdûbi aleyhim ve leddâllîn. Amîn"),
        ]
        self.totalZhikirCount = self.zhikirData.reduce(0) { c, z in
            return c + z.Count }
    }
    
    func prepareSpinner() {
        self.ZhikirTargetView.encircle()
        SwiftSpinner.useContainerView(self.ZhikirTargetView)
        SwiftSpinner.setTitleFont(UIFont(name: "OpenSans-Light", size: 17.0))
    }
    
}




