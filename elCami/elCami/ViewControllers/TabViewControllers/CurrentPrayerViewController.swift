//
//  CurrentPrayerViewController.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 06/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import UIKit

class CurrentPrayerViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var ActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var ClockView: UIView!
    @IBOutlet weak var CircleView: UIView!
    @IBOutlet weak var DigitalView: UIView!
    @IBOutlet weak var LocationView: UIView!
    
    @IBOutlet weak var ArrivingPrayerLabel: UILabel!
    @IBOutlet weak var ArrivingHourLabel: UILabel!
    @IBOutlet weak var ArrivingMinuteLabel: UILabel!
    
    @IBOutlet weak var secondsIndicator: UILabel!
    @IBOutlet weak var DateTimeLabel: UILabel!
    
    @IBOutlet weak var CurrentStateLabel: UILabel!
    @IBOutlet weak var CurrentCityLabel: UILabel!
    
    @IBOutlet weak var PrayerTimesView: UIView!
    @IBOutlet weak var FajrLabel: UILabel!
    @IBOutlet weak var SunLabel: UILabel!
    @IBOutlet weak var ZuhrLabel: UILabel!
    @IBOutlet weak var AsrLabel: UILabel!
    @IBOutlet weak var MaghhribLabel: UILabel!
    @IBOutlet weak var IshaLabel: UILabel!
    
    @IBOutlet weak var FajrTitle: UILabel!
    @IBOutlet weak var SunTitle: UILabel!
    @IBOutlet weak var ZuhrTitle: UILabel!
    @IBOutlet weak var AsrTitle: UILabel!
    @IBOutlet weak var MaghhribTitle: UILabel!
    @IBOutlet weak var IshaTitle: UILabel!
    
    @IBOutlet weak var AyatHadithTranscript: UILabel!
    
    // MARK: - Declarations
    
    var salatClock: SalatClockView!
    var prayerTimes: PrayerTimes?
    let prayerTimesEngine = PrayerTimesEngine()
    let AyatHadith = AyatHadithEngine()
    var removedToForeground = false
    var availableCities = [CoreCity]() {
        didSet {
            DispatchQueue.main.async {
                if self.availableCities.count > 0 {
                    let city = self.availableCities[self.cityIndex]
                    self.CurrentCityLabel.text = city.name!
                    self.CurrentStateLabel.text = city.state!.name!
                    self.prayerTimesEngine.getPrayerTimes(byCoreCity: city, callback: { (times) in
                        self.prayerTimes = times
                        self.prepareSalatClock()
                        self.prepareArrivingPrayerClock()
                        self.prepareAyatHadith()
                        self.prepareLabels()
                        self.ActivityIndicator.stopAnimating()
                        if let blur = (self.view.subviews.filter { $0.tag == 571 }).first {
                            blur.removeFromSuperview()
                        }
                    })
                } else {
                    self.prayerTimesEngine
                        .getPrayerTimes(by: "TÜRKİYE",
                                        stateName: self.CurrentStateLabel.text!,
                                        cityName: self.CurrentCityLabel.text!)
                        {
                            times in
                            self.prayerTimes = times
                            self.prepareSalatClock()
                            self.prepareArrivingPrayerClock()
                            self.prepareAyatHadith()
                            self.prepareLabels()
                            self.ActivityIndicator.stopAnimating()
                            if let blur = (self.view.subviews.filter { $0.tag == 571 }).first {
                                blur.removeFromSuperview()
                            }
                    }
                }
            }
        }
    }
    var cityIndex = 0 {
        didSet {
            if self.cityIndex == self.availableCities.count {
                self.cityIndex = 0
            } else if self.cityIndex == -self.availableCities.count {
                self.cityIndex = self.availableCities.count - 1
            }
        }
    }
    // MARK: - Lifecycle Handlers
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: Notification.Name.UIApplicationWillResignActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToForeground), name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
        self.preparePrayerTimes()
    }
    
    // MARK: - Private Functions
    @objc private func appMovedToBackground() {
        if let clock = self.salatClock {
            clock.stopClock()
        }
    }
    
    @objc private func appMovedToForeground() {
        self.preparePrayerTimes()
    }
    
    // MARK: - Preparation Functions
    func prepareCoreCities() {
        guard let app = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        app.parseIdentity { (identity) in
            self.availableCities = identity.cities?.allObjects as! [CoreCity]
        }
    }
    func prepareSpinner() {
        let blurEffect = UIBlurEffect(style: .light)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = self.view.frame
        blurView.tag = 571
        self.view.addSubview(blurView)
        self.view.bringSubview(toFront: blurView)
        self.view.bringSubview(toFront: self.ActivityIndicator)
        self.ActivityIndicator.startAnimating()
    }
    func preparePrayerTimes() {
        self.prepareCoreCities()
        self.prayerTimesEngine.subscribeToFetchPostEngine { (fte, type) in
            self.ActivityIndicator.stopAnimating()
            if let blur = (self.view.subviews.filter { $0.tag == 571 }).first {
                blur.removeFromSuperview()
            }
        }
        DispatchQueue.main.async {
            self.prepareSpinner()
        }
        
    }
    func prepareAyatHadith() {
//        AyatHadith.dailyHadith { (Hadith) in
//            self.AyatHadithTranscript.text = Hadith.Transcript!
//        }
    }

    func prepareSalatClock() {
        self.salatClock?.stopClock()
        self.salatClock = SalatClockView(frame: self.CircleView.frame)
        self.salatClock!.setPrayerTimes(prayerTimes)
        self.salatClock!.backgroundColor = UIColor.clear
        self.CircleView.addSubview(self.salatClock!)
        self.salatClock!.startClock()
        
    }
    
    func prepareArrivingPrayerClock() {
        if let times = self.prayerTimes {
            let hourTimer = Timer(fire: Date(), interval: TimeInterval(60), repeats: true) {t in
                let current = ClockAngles(date: Date())
                let arriving = ClockAngles(prayerTime: times.arrivingPrayer)
                self.ArrivingHourLabel.text = (arriving - current).digitalized()
            }
            let minuteTimer = Timer(fire: Date(), interval: TimeInterval(1), repeats: true) {t in
                let current = ClockAngles(date: Date())
                let arriving = ClockAngles(prayerTime: times.arrivingPrayer)
                self.ArrivingMinuteLabel.text = (arriving -- current).digitalized()
            }
            RunLoop.main.add(hourTimer, forMode: RunLoopMode.commonModes)
            RunLoop.main.add(minuteTimer, forMode: RunLoopMode.commonModes)
            UIView.animate(withDuration: TimeInterval(1), delay: 0.0, options: .repeat, animations: {
                if self.secondsIndicator.layer.opacity == 1.0 {
                    self.secondsIndicator.layer.opacity = 0.0
                } else {
                    self.secondsIndicator.layer.opacity = 1.0
                }
                
            }, completion: nil)
        }
        
    }

    func prepareLabels() {
        if self.removedToForeground {
            return
        }
        if let times = self.prayerTimes {
            self.ArrivingPrayerLabel.text = times.arrivingPrayer.Name.description

            self.FajrLabel.text = times.Fajr.clock
            self.SunLabel.text = times.Sun.clock
            self.ZuhrLabel.text = times.Zuhr.clock
            self.AsrLabel.text = times.Asr.clock
            self.MaghhribLabel.text = times.Maghhrib.clock
            self.IshaLabel.text = times.Isha.clock
            
            for (label, time) in [
                (self.FajrTitle!, times.Fajr),
                (self.SunTitle!, times.Sun),
                (self.ZuhrTitle!, times.Zuhr),
                (self.AsrTitle!, times.Asr),
                (self.MaghhribTitle!, times.Maghhrib),
                (self.IshaTitle!, times.Isha)] {
                label.textColor = time.DefinedColor
                label.addLegendView(withColor: time.DefinedColor,
                                    shape: .circle,
                                    margin: -3.0,
                                    scaleTo: 0.3)
            }
            
            self.DateTimeLabel.text = Date().explain(calendar: Calendar.current)
            self.PrayerTimesView.layer.addBorder(edge: .left, color: UIColor(colorLiteralRed: 0.0, green: 128/255, blue: 64/255, alpha: 1.0), thickness: 1.0, padding: 5.0)
            self.salatClock.subscribe(.onClockDrawn) { c in
                
            }
            self.LocationView.addSwipeGestureHandler(action: #selector(didSwipeToIncrement))
        }
    }
    func didSwipeToIncrement(swipe: UISwipeGestureRecognizer) {
        let increment = swipe.direction == .right ? 1 : -1
        self.cityIndex += increment
        self.preparePrayerTimes()
    }

}

extension CurrentPrayerViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
