//
//  CountriesTableViewController.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 12/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import UIKit
import CoreData

class CountriesTableViewController: UITableViewController, UISearchBarDelegate, DataProviderDelegate {
    
    typealias Element = CoreCountry
    typealias TOwner = CoreCountry
    
    // MARK: - Outlets
    @IBOutlet weak var ActivityView: UIActivityIndicatorView!
    @IBOutlet weak var SearchView: UISearchBar!
    
    // MARK: - Declarations
    var owner: CoreCountry? = nil
    var data: [CoreCountry]?
    var filtered: [CoreCountry]?
    
    // MARK: - Config Functions
    func configureSearchController() {
        // Initialize and perform a minimum configuration to the search controller.
        self.SearchView.searchBarStyle = .prominent
        self.SearchView.placeholder = "Devletler..."
        self.SearchView.sizeToFit()
        
        // Place the search bar view to the tableview headerview.
        self.tableView.tableHeaderView = self.SearchView
    }
    
    // MARK: - Lifecycle Handle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        
        self.configureSearchController()
        self.configureKeypad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Country.seed(
            collect: { results in
                self.data = results
                self.filtered = results
                self.tableView.reloadData()
                self.ActivityView.stopAnimating()
        }) { (fpe, type, err) in
            self.ActivityView.stopAnimating()
        }
    }
    
    // MARK: - Search Overrides
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filtered = self.data?.filter { $0.name?[0, searchText.characters.count].lowercased() == searchText.lowercased() }
        self.tableView.reloadData()
    }
    
    // MARK: - Table View Overrides
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.filtered?.filter({ $0.favorite }).count ?? 0
        case 1:
            return self.filtered?.filter({ !$0.favorite }).count ?? 0
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Sık Kullanılanlar"
        case 1:
            return "Tamamı"
        default:
            return "Tanımsız"
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.configure(by: self.filtered, at: indexPath, sectionFilter: { $0.favorite }, textParser: { $0.name ?? "Veri Yok" })
        return cell
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        return self.parseActions(
            config: [("Çıkar", UIColor.red, indexPath.section == 0, { $0.favorite }, { $0.favorite = false }),
                     ("Ekle", UIColor.blue, indexPath.section == 1, { !$0.favorite }, { $0.favorite = true })],
            at: indexPath
        )
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: SegueIdentifiers.ShowStateIdentifier, sender: self)
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            
            switch(indexPath.section) {
            case 0:
                let country = Country(name: "", code: "")
                if let cc = country.store(favorite: true) {
                    self.data?.append(cc)
                }
            case 1:
                let country = Country(name: "", code: "")
                if let cc = country.store(favorite: false) {
                    self.data?.append(cc)
                }
                
            default:
                return
            }
            
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: [
                IndexPath(item: indexPath.row, section: indexPath.section)
                ], with: .right)
            self.tableView.endUpdates()
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let id = segue.identifier {
            if id == SegueIdentifiers.ShowStateIdentifier {
                if let destination = segue.destination as? StatesTableViewController {
                    if let data = self.filtered, let dataFavorites = self.filtered?.filter({ $0.favorite }),
                        let indexPath = tableView.indexPathForSelectedRow {
                        destination.owner = indexPath.section == 0 ? dataFavorites[indexPath.row] : data[indexPath.row]
                    }
                    
                }
            }
        }
    }
    
    
}
