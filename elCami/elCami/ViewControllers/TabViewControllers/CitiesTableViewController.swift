//
//  CitiesTableViewController.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 13/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import UIKit

class CitiesTableViewController: UITableViewController, DataProviderDelegate {
    
    typealias Element = CoreCity
    typealias TOwner = CoreState
    
    // MARK: - Outlets
    @IBOutlet weak var ActivityView: UIActivityIndicatorView!
    
    
    // MARK: - Declarations
    var owner: CoreState? = nil
    var data: [CoreCity]?
    var filtered: [CoreCity]?
    
    
    // MARK: - Lifecycle Handlers
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let owner = self.owner {
            City.seed(
                byState: owner,
                collect: { results in
                    self.data = results
                    self.filtered = results
                    self.tableView.reloadData()
                    self.ActivityView.stopAnimating()
            }) { (fpe, type, err) in
                self.ActivityView.stopAnimating()
            }
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.filtered?.filter({ $0.added }).count ?? 0
        case 1:
            return self.filtered?.filter({ !$0.added }).count ?? 0
        default:
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.configure(by: self.filtered,
                       at: indexPath,
                       sectionFilter: { $0.added },
                       textParser: { $0.name ?? "Veri Yok" },
                       commit: { $0.accessoryType = $1.added ? .checkmark : .none })
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Seçilenler"
        case 1:
            return "Tamamı"
        default:
            return "Tanımsız"
        }
    }
    
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let app = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        for cell in tableView.visibleCells {
            let idx = tableView.indexPath(for: cell)
            if idx == indexPath {
                cell.configure(by: self.filtered, at: indexPath, sectionFilter: { $0.added }, textParser: { $0.name ?? "Veri Yok" })
                switch(indexPath.section) {
                case 0:
                    //already added, delete it
                    if let coreCity = self.filtered?.filter({ $0.added })[indexPath.row] {
                        cell.accessoryType = .none
                        app.parseIdentity(callback: { (identity) in
                            coreCity.added = false
                            identity.removeFromCities(coreCity)
                            app.saveContext()
                            self.tableView.reloadData()
                        })

                    }
                case 1:
                    //not added, add it
                    if let coreCity = self.filtered?.filter({ !$0.added })[indexPath.row] {
                        cell.accessoryType = .checkmark
                        cell.accessoryView?.tintColor = UIColor.blue
                        app.parseIdentity(callback: { (identity) in
                            coreCity.added = true
                            identity.addToCities(coreCity)
                            app.saveContext()
                            self.tableView.reloadData()
                        })
                    }
                default:
                    break
                }
            }
        }
    }

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
