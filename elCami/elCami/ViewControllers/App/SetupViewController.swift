//
//  SetupViewController.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 12/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import UIKit

class SetupViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var ActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var ActivitySubTitle: UILabel!
    
    
    // MARK: - Lifecycle Handlers
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareInit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.prepareDatabase()
    }
    
    // MARK: - Preparation Functions
    private func prepareInit() {
        self.ActivitySubTitle.text = "Bismillahirrahmanirrahim..."
        ActivityIndicator.startAnimating()
    }
    private func prepareDatabase() {
        DispatchQueue.main.async { 
            if let app = UIApplication.shared.delegate as? AppDelegate {
                //app.clearCoreDataStore()
                self.navigateForward()
            }
        }
    }
    
    // MARK: - Private Function
    private func navigateForward() {
        DispatchQueue.main.async {
            self.ActivityIndicator.stopAnimating()
            self.performSegue(withIdentifier: SegueIdentifiers.ShowTabViewIdentifier, sender: self)
        }
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let id = segue.identifier {
//        }
//    }
//    

}
