//
//  AyatHadithEngine.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 10/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation

class AyatHadithEngine {
    
    let scraper = ScraperEngine(site: URL(string: PredefinedUrls.AyatHadithUrl))
    
    func dailyHadith(callback: @escaping (Hadith) -> Void) {
        scraper.process { (doc) in
            if let element = doc.css(".entry-content.clearfix.standard-content").random {
                if let text = element.text {
                    var indexes = text.indexes(of: "\n")
                    if indexes.count > 1 {
                        let transcript = text.substring(with: Range(uncheckedBounds: (lower: indexes[0], upper: indexes[1])))
                        callback(Hadith(transcript: transcript[2, transcript.characters.count - 1]))
                    }
                }
            }
        }
    }
}
