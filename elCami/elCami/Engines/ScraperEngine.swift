//
//  ScraperEngine.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 03/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation
import Kanna

enum ScraperEngineError: Error {
    case NoDocumentFrom(from: URL?)
}

class ScraperEngine {
    
    private var failbacks = [((ScraperEngine, [Error])-> Void)]()
    private var connectionErrors = [Error]()
    private var doc: HTMLDocument? = nil
    private let site: URL!
    
    init(site: URL!) {
        self.site = site
    }
    
    private func connect(to site: URL) {
        var html = ""
        do {
            html = try String(contentsOf: site, encoding: .utf8)
            doc = HTML(html: html, encoding: .utf8)
        } catch {
            do {
                html = try String(contentsOf: site, encoding: .ascii)
                doc = HTML(html: html, encoding: .ascii)
            } catch let error {
                self.connectionErrors.append(error)
            }
        }
    }
    
    func process(callback: @escaping (_ document: HTMLDocument) -> Void) {
        self.connect(to: self.site)
        if let d = doc {
            callback(d)
        } else {
            let _ = self.failbacks.map { $0(self, self.connectionErrors + [ScraperEngineError.NoDocumentFrom(from: self.site)]) }
        }
    }
    
    func subscribe(failback: @escaping (ScraperEngine, [Error])-> Void) {
        self.failbacks.append(failback)
    }
    
    
}
