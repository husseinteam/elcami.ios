//
//  XMLElementExtensions.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 04/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation
import Kanna

extension XMLElement {
    
    func attributes() -> [String:String]! {
        var words: [String:String]! = [:]
        var h = self.toHTML!
        h = h[h.index(h.startIndex, offsetBy: 1)..<h.characters.index(of: Character(">"))!]
        
        let components = h.components(separatedBy: " ")
        for component in components {
            let _ = h.range(of: component)
            if component.contains("=") {
                let attrval = component.components(separatedBy: "=")
                words[attrval[0]] = attrval[1].replacingOccurrences(of: "\"", with: "").replacingOccurrences(of: "'", with: "")
            }
        }
        
        return words
    }
    
    func hasAttribute(key: String) -> Bool {
        return self.attributes().keys.contains(key)
    }
    
}

extension XPathObject {

    var random: XMLElement? {
        get {
            let randomIndex = Int(arc4random_uniform(UInt32(self.underestimatedCount)))
            return self[randomIndex]
        }
    }
    
}
