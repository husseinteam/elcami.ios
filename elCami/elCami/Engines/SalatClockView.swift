//
//  AnalogClockView.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 06/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import UIKit

enum DrawBy {
    case Hour
    case Minute
    case Second
}

enum SalatClockEvents {
    case onClockDrawn
}

class SalatClockView: UIView {
    
    var prayerTimes: PrayerTimes?
    
    var seconds = [UILabel]()
    var minutes = [UILabel]()
    var hours = [UILabel]()
    var prayerArcDict = [PrayerNames: CAShapeLayer]()
    
    var hoursArc = CAShapeLayer()
    var minutesArc = CAShapeLayer()
    var secondsArc = CAShapeLayer()
    
    var hoursRect: CGRect? = nil
    var minutesRect: CGRect? = nil
    var secondsRect: CGRect? = nil
    
    var _running: Bool = false
    var secondsTimer: Timer? = nil
    var minutesTimer: Timer? = nil
    
    var hoursLabeler: () -> UILabel = {
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 29.0, height: 21.0))
        label.font = UIFont.init(name: "SquareSansSerif7", size: 9.0)
        label.textColor = UIColor.white
        label.shadowColor = UIColor.black
        label.layer.shouldRasterize = true
        label.textAlignment = NSTextAlignment.center
        return label
    }
    var minutesLabeler: () -> UILabel = {
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 25.0, height: 18.0))
        label.font = UIFont.init(name: "SquareSansSerif7", size: 9.0)
        label.textColor = UIColor(hash: "#FFB100", alpha: 1.0)
        label.textAlignment = NSTextAlignment.center
        return label
    }
    var secondsLabeler: () -> UILabel = {
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 13.0, height: 9.0))
        label.font = UIFont.init(name: "OpenSans-Light", size: 7.0)
        label.textColor = UIColor.green
        label.textAlignment = NSTextAlignment.center
        return label
    }
    
    var hoursRadius: CGFloat! = 0.0
    var minutesRadius: CGFloat! = 0.0
    var secondsRadius: CGFloat! = 0.0
    var timesRadius: CGFloat! = 0.0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        self.drawClock(rect: rect)
    }
    
    func drawClock(rect: CGRect) {
        
        self.hoursArc.removeFromSuperlayer()
        self.minutesArc.removeFromSuperlayer()
        self.secondsArc.removeFromSuperlayer()
        
        for nlabel in self.seconds + self.minutes + self.hours  {
            nlabel.removeFromSuperview()
        }
        
        hoursRect = rect.magnified(by: 0.99)
        minutesRect = rect.magnified(by: 0.79)
        secondsRect = rect.magnified(by: 0.61)
        
        self.secondsRadius = self.secondsRect!.size.width / 2.0 * 0.897
        self.minutesRadius = self.minutesRect!.size.width / 2.0 * 0.897
        self.hoursRadius = self.hoursRect!.size.width / 2.0 * 0.897
        self.timesRadius = self.hoursRadius * 1.14
        
        let hourNumbers = [24] + stride(from: 1, to: 24, by: 1)
        let minuteNumbers = [60] + stride(from: 5, to: 60, by: 5)
        let secondsNumbers = [60] + stride(from: 2, to: 60, by: 2)
     
        for number in hourNumbers {
            let label: UILabel = self.hoursLabeler()
            label.text = "\(number)"
            self.hours.append(label)
            self.addSubview(label)
        }
        for number in minuteNumbers {
            let label: UILabel = self.minutesLabeler()
            label.text = "\(number)"
            self.minutes.append(label)
            self.addSubview(label)
        }
        for number in secondsNumbers {
            let label: UILabel = self.secondsLabeler()
            label.text = "\(number)"
            self.seconds.append(label)
            self.addSubview(label)
        }
        self.drawHoursIndicator()
        self.animateTimesCircle(rect: self.hoursRect!, by: Date())
        self.onDrawnCallback?(self)
        
    }
    
    func drawHoursIndicator() {
        let arcWidth = (.pi / 4) / 18.0
        let arcPath = UIBezierPath(arcCenter: self.superview!.convert(self.center, to: self),
                                   radius: self.timesRadius * 0.7,
                                   startAngle: CGFloat(.pi - arcWidth),
                                   endAngle: CGFloat(.pi + arcWidth),
                                   clockwise: true)
        let arc = CAShapeLayer()
        arc.path = arcPath.cgPath
        arc.strokeColor = UIColor.yellow.cgColor
        arc.fillColor = UIColor.clear.cgColor
        arc.lineWidth = self.timesRadius * 0.57
        arc.opacity = 0.5
        self.layer.superlayer!.insertSublayer(arc, below: self.layer)
    }
    
    func animateTimesCircle(rect: CGRect, by date: Date) {
        
        if let times = self.prayerTimes {
            for t in [times.Fajr, times.Sun, times.Zuhr, times.Asr, times.Maghhrib, times.Isha] {
                prayerArcDict[t.Name]?.removeFromSuperlayer()
            }
            
            prayerArcDict = [:]
            let tlist = [times.Fajr, times.Sun, times.Zuhr, times.Asr, times.Maghhrib, times.Isha]
            let nowAngle = ClockAngles(date: date)
            for time in tlist {
                let startAngle = ClockAngles(prayerTime: time)
                let endAngle = ClockAngles(prayerTime: time.NextPrayerTime)
                let arcPath = UIBezierPath(arcCenter: self.superview!.convert(self.center, to: self),
                                           radius: self.timesRadius,
                                           startAngle: CGFloat(nowAngle >- startAngle + .pi),
                                           endAngle: CGFloat(nowAngle >- endAngle + .pi),
                                           clockwise: false)
                let arc = CAShapeLayer()
                arc.path = arcPath.cgPath
                arc.strokeColor = time.DefinedColor.cgColor
                arc.fillColor = UIColor.clear.cgColor
                arc.lineWidth = 3.4
                arc.shadowColor = UIColor.white.cgColor
                arc.shadowRadius = 3.0
                if (prayerArcDict.keys.contains(time.Name)) {
                    prayerArcDict[time.Name]?.removeFromSuperlayer()
                }
                prayerArcDict[time.Name] = arc
                self.layer.superlayer!.insertSublayer(arc, below: self.layer)
            }
        }
    }
    
    func setPrayerTimes(_ prayerTimes: PrayerTimes?) {
        self.prayerTimes = prayerTimes
    }
    
    func animateNumbers(_ numbers: [UILabel],
                        in area: CGRect?,
                        to angle: Double,
                        with radius: CGFloat) {
        if let rect = area {
            UIView.animate(withDuration: TimeInterval(1), delay: 0,
                           options: UIViewAnimationOptions.allowUserInteraction,
               
                           animations: {
                            for (idx, l) in numbers.enumerated() {
                                let delta = 2 * .pi*Double(idx)/Double(numbers.count)
                                let x = cosf(Float(angle - delta)) * Float(radius) + Float(rect.midX)
                                let y = sinf(Float(angle - delta)) * Float(radius) + Float(rect.midY)
                                let center = CGPoint(x: CGFloat(x), y: CGFloat(y))
                                l.center = self.superview!.convert(center, to: self)
                            }
            },
                           completion: nil)
        }
    }
    
    func setTimeToDate(date: Date) {
        let angles = ClockAngles(date: date)
        self.animateNumbers(self.seconds,
                            in: self.secondsRect,
                            to: angles.SecondAngle,
                            with: self.secondsRadius)
        DispatchQueue.main.async( execute: {
            self.animateNumbers(self.minutes,
                                in: self.minutesRect,
                                to: angles.MinuteAngle,
                                with: self.minutesRadius)
        })
        DispatchQueue.main.async( execute: {
            self.animateNumbers(self.hours,
                                in: self.hoursRect,
                                to: angles.HourAngle,
                                with: self.hoursRadius)
        })
        
    }
    
    func startClock() {
        if (!_running) {
            _running = true
            self.secondsTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(1), repeats: true, block: { (timer) in
                self.setTimeToDate(date: Date())
            })
            self.minutesTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(60), repeats: true, block: { timer in
                if let rect = self.hoursRect {
                    self.animateTimesCircle(rect: rect, by: Date())
                }
            })
        }
    }
    
    func stopClock() {
        if (_running) {
            _running = false
            self.secondsTimer?.invalidate()
            self.minutesTimer?.invalidate()
            let _ = self.seconds.map { $0.removeFromSuperview() }
            let _ = self.minutes.map { $0.removeFromSuperview() }
            let _ = self.hours.map { $0.removeFromSuperview() }
            
        }
    }
    
    // MARK - Subscription
    private var onDrawnCallback: ((SalatClockView) -> Void)? = nil
    func subscribe(_ eventType: SalatClockEvents, callback: @escaping (SalatClockView) -> Void) {
        switch eventType {
        case .onClockDrawn:
            self.onDrawnCallback = callback
        }
    }
}



