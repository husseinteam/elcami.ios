//
//  FetchPostEngine.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 04/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

enum FetchPostType {
    case fetch(from: String)
    case post(to: String)
}

class FetchPostEngine {
    
    private var fetchRequest: DataRequest? = nil
    private var postRequest: DataRequest? = nil
    private var callbacks = [((FetchPostEngine, FetchPostType)-> Void)]()
    private var failbacks = [((FetchPostEngine, FetchPostType, Error)-> Void)]()
    
    func fetchJSON(from: String, callback: @escaping((JSON) -> Void)) {
        self.fetchRequest = Alamofire.request(from)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    callback(json)
                    let _ = self.callbacks.map { $0(self, .fetch(from: from)) }
                case .failure(let error):
                    let _ = self.failbacks.map { $0(self, .fetch(from: from), error) }
                }
        }
    }
    func postJSON(to: String, parameters: [String: String], callback: @escaping((JSON) -> Void)) {
        self.postRequest = Alamofire.request(to, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    callback(json)
                    let _ = self.callbacks.map { $0(self, .post(to: to)) }
                case .failure(let error):
                    let _ = self.failbacks.map { $0(self, .post(to: to), error) }
                }
        }
    }
    
    func subscribe(callback: @escaping (FetchPostEngine, FetchPostType)-> Void) {
        self.callbacks.append(callback)
    }
    
    func subscribe(failback: @escaping (FetchPostEngine, FetchPostType, Error)-> Void) {
        self.failbacks.append(failback)
    }
    
    func abortFetchRequest() {
        self.fetchRequest?.cancel()
    }
    
    func abortPostRequest() {
        self.postRequest?.cancel()
    }
    
}
