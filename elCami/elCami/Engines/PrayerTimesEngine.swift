//
//  PrayerTimesEngine.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 06/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation
import SwiftyJSON

class PrayerTimesEngine {
    
    let fetchPostEngine = FetchPostEngine()
    let scraper = ScraperEngine(
        site: URL(string: PredefinedUrls.PrayerUrl))
    
    func subscribeToFetchPostEngine(callback: @escaping (FetchPostEngine, FetchPostType) -> Void) {
        self.fetchPostEngine.subscribe(callback: callback)
    }
    
    func subscribeToFetchPostEngine(failback: @escaping (FetchPostEngine, FetchPostType, Error) -> Void) {
        self.fetchPostEngine.subscribe(failback: failback)
    }
    
    func getCountries() -> [Country] {
        var countries = [Country]()
        if let file = Bundle.main.path(forResource: "countries", ofType: "txt") {
            do {
                let txt = try String(contentsOfFile: file)
                for line in txt.components(separatedBy: "\n") {
                    if line.contains(",") {
                        let codeName = line.components(separatedBy: ",")
                        let(name, code) = (codeName[1], codeName[0])
                        countries.append(Country(name: name, code: code))
                    }
                }
            } catch {
                fatalError("Error whilst countries resource extraction")
            }
        }
        return countries.filter { $0.Code != "" }
    }
    
    func getCountry(byName countryName: String, foundBack: @escaping (Country) -> Void) {
        if let country = (self.getCountries().filter { $0.Name == countryName }.first) {
            foundBack(country)
        }
    }
    
    func getStates(by country: Country, noneback: (() -> Void)? = nil, callback: @escaping ([State]) -> Void) {
        fetchPostEngine.fetchJSON(from: "\(PredefinedUrls.FillStateRoute)\(country.Code)") { json in
            var states = [State]()
            for (_,state):(String, JSON) in json {
                states.append(State(name: state["Text"].string, code: state["Value"].string))
            }
            callback(states)
            if states.count == 0 {
                noneback?()
            }
        }
    }
    
    func getCities(by state: State, noneback: (() -> Void)? = nil, callback: @escaping ([City]) -> Void) {
        fetchPostEngine.fetchJSON(from: "\(PredefinedUrls.FillCityRoute)\(state.Code)") { json in
            var cities = [City]()
            for (_,city):(String, JSON) in json {
                cities.append(City(name: city["Text"].string, code: city["Value"].string))
            }
            callback(cities)
            if cities.count == 0 {
                noneback?()
            }
        }
    }
    
    func getPrayerTimes(by country: Country, state: State, city: City, callback: @escaping (PrayerTimes?) -> Void) {
        fetchPostEngine.postJSON(to: PredefinedUrls.PrayerTimesSetRoute, parameters: ["countryName": country.Code, "stateName": state.Code, "name": city.Code]) { response in
            callback(PrayerTimes(json: response))
        }
    }
    
    func getPrayerTimes(by countryCode: String, stateCode: String, cityCode: String, callback: @escaping (PrayerTimes?) -> Void) {
        fetchPostEngine.postJSON(to: PredefinedUrls.PrayerTimesSetRoute, parameters: ["countryName": countryCode, "stateName": stateCode, "name": cityCode]) { response in
            callback(PrayerTimes(json: response))
        }
    }
    func getPrayerTimes(byCoreCity: CoreCity, callback: @escaping (PrayerTimes?) -> Void) {
        fetchPostEngine.postJSON(to: PredefinedUrls.PrayerTimesSetRoute,
                                 parameters: [
                                    "countryName": byCoreCity.state!.country!.code!,
                                    "stateName": byCoreCity.state!.code!,
                                    "name": byCoreCity.code!])
        { response in
            callback(PrayerTimes(json: response))
        }
    }
    func getPrayerTimes(by countryName: String, stateName: String, cityName: String, callback: @escaping (PrayerTimes?) -> Void) {
        self.getCountry(byName: countryName) { country in
            self.getStates(by: country) { states in
                if let state = (states.filter { $0.Name == stateName }.first) {
                    self.getCities(by: state) { cities in
                        if let city = (cities.filter {
                            $0.Name == (cityName.uppercased() == "MERKEZ" ? state.Name : cityName)
                            }.first) {
                            self.getPrayerTimes(by: country, state: state, city: city) { times in
                                callback(times)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func abortFetchOperation() {
        self.fetchPostEngine.abortFetchRequest()
    }
    
    func abortPostOperation() {
        self.fetchPostEngine.abortPostRequest()
    }
    
}


