//
//  File.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 13/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    private func loopThrough<V: UIView>( list: inout [V],
                             that: ((UIView) -> Bool)? = nil) {
        for view in self.subviews as [UIView] {
            if view is V {
                if let filter = that {
                    if filter(view) { list.append(view as! V) }
                } else {
                    list.append(view as! V)
                }
            } else {
                view.loopThrough(list: &list)
            }
        }
    }
    func findAllSubViews() -> [UIView] {
        var allViews = [UIView]()
        self.loopThrough(list: &allViews)
        return allViews
    }
    func matchFirstSubView(that: @escaping (UIView) -> Bool) -> UIView? {
        var allViews = [UIView]()
        self.loopThrough(list: &allViews, that: that)
        return allViews.first
    }
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    
}

extension UIView {

    func addPanGestureHandler(action: Selector) {
        let target = self.parentViewController
        let pan = UIPanGestureRecognizer(target: target,
                                             action: action)
        pan.delegate = target as? UIGestureRecognizerDelegate
        self.addGestureRecognizer(pan)
    }
    
    func addSwipeGestureHandler(action: Selector) {
        let target = self.parentViewController
        let swipe = UISwipeGestureRecognizer(target: target,
                                             action: action)
        swipe.delegate = target as? UIGestureRecognizerDelegate
        self.addGestureRecognizer(swipe)
    }
}

extension UIViewController {
    func setKeyPadHidden(onTap: UIView) {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeypad))
        onTap.addGestureRecognizer(tap)
    }
    
    func dismissKeypad() {
        self.view.endEditing(true)
    }
    func configureKeypad() {
        let field = UIResponder.currentFirstResponder()
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(UIViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        field?.inputAccessoryView?.addSubview(doneToolbar)
    }
    
    func doneButtonAction() {
        let field = UIResponder.currentFirstResponder()
        field?.resignFirstResponder()
    }
}

extension UIPanGestureRecognizer {
    
    enum PanDirection {
        case right
        case left
    }
    
    func direction(in view: UIView) -> PanDirection {
        let v: CGPoint = velocity(in: view)
        if v.x > 0 {
            return .right
        } else {
            return .left
        }
    }
}

extension UITableViewCell {
    
    func configure<TCoreData>(by data: [TCoreData]?,
                   at indexPath: IndexPath,
                   sectionFilter: (TCoreData) -> Bool,
                   textParser: (TCoreData) -> String,
                   commit: ((UITableViewCell, TCoreData) -> Void)? = nil) {
        self.textLabel?.font = UIFont(name: "OpenSans-Light", size: 13.0)
        self.textLabel?.minimumScaleFactor = 0.5
        switch(indexPath.section) {
        case 0:
            if let data = data?.filter({ sectionFilter($0) }) {
                self.textLabel?.text = textParser(data[indexPath.row])
                commit?(self, data[indexPath.row])
            }
        case 1:
            if let data = data?.filter({ !sectionFilter($0) }) {
                self.textLabel?.text = textParser(data[indexPath.row])
                commit?(self, data[indexPath.row])
            }
        default:
            break
        }
    }
    
}

extension UICollectionViewCell {
    
    func configure<TCoreData>(by data: [TCoreData]?,
                   at indexPath: IndexPath,
                   sectionFilter: (TCoreData) -> Bool,
                   textParser: (TCoreData) -> String,
                   commit: ((UITableViewCell, TCoreData) -> Void)? = nil) {
//        self.contentView.?.font = UIFont(name: "OpenSans-Light", size: 13.0)
//        self.textLabel?.minimumScaleFactor = 0.5
//        switch(indexPath.section) {
//        case 0:
//            if let data = data?.filter({ sectionFilter($0) }) {
//                self.textLabel?.text = textParser(data[indexPath.row])
//                commit?(self, data[indexPath.row])
//            }
//        case 1:
//            if let data = data?.filter({ !sectionFilter($0) }) {
//                self.textLabel?.text = textParser(data[indexPath.row])
//                commit?(self, data[indexPath.row])
//            }
//        default:
//            break
//        }
    }
    
}

extension UIResponder {
    private weak static var _currentFirstResponder: UIResponder? = nil
    
    public class func currentFirstResponder() -> UIResponder? {
        UIResponder._currentFirstResponder = nil
        UIApplication.shared.sendAction(#selector(UIResponder.findFirstResponder), to: nil, from: nil, for: nil)
        return UIResponder._currentFirstResponder
    }
    
    internal func findFirstResponder(sender: AnyObject) {
        UIResponder._currentFirstResponder = self
    }
}






