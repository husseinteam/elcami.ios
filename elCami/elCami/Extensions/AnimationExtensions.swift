//
//  AnimationExtensions.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 12/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation
import UIKit

extension CALayer {
    
    func rotate(by angle: Double, radius: CGFloat) {
        var transform = CATransform3DIdentity
        transform = CATransform3DTranslate(transform, radius, radius, 0)
        transform = CATransform3DRotate(transform, CGFloat(angle), 0, 0, 1.0)
        transform = CATransform3DTranslate(transform, -radius, -radius, 0)
        self.transform = transform
    }
    
}

extension UIView {
    
    func animateBubble(alphaBegin: CGFloat, alphaEnd: CGFloat, upperScale: CGFloat, withDuration: Double = 0.05) {

        self.alpha = alphaBegin
        UIView.animate(withDuration: TimeInterval(withDuration), delay: TimeInterval(0), options: .transitionFlipFromBottom, animations: {
            self.alpha = alphaEnd
            self.transform = self.transform.scaledBy(x: upperScale, y: upperScale)
            
        }) { done in
            UIView.animate(withDuration: TimeInterval(withDuration)) {
                self.alpha = alphaBegin
                self.transform = CATransform3DGetAffineTransform(CATransform3DIdentity)
            }
        }
        
    }
    
}
