//
//  ClockExtensions.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 09/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    func digitalized() -> String {
        if let i = Int(self) {
            return Int(i) < 10 ? "0\(Int(i))" : "\(Int(i))"
        } else {
            return self
        }
    }
    
}

extension Int {
    
    func digitalized() -> String  {
        return self < 10 ? "0\(self)" : "\(self)"
    }
    
}

extension Date {
    
    func explain(calendar: Calendar) -> String {
        //calendar.firstWeekday -> Sunday == 1, Saturday == 7
        let data = calendar.dateComponents([.day, .month, .year, .weekday], from: self)
        let months = ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"]
        let days = ["Cuma", "Cumartesi", "Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe"]
        return "\(data.day?.digitalized() ?? "31") \(months[(data.month ?? 11)-1]) \(data.year ?? 1982) \(days[(data.weekday! + 1) % 7])"
    }
    
}
