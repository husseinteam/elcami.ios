//
//  Operators.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 10/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation

precedencegroup ClockPrecedence {
    higherThan: AdditionPrecedence
    associativity: left
}

infix operator -- : ClockPrecedence
infix operator - : ClockPrecedence
infix operator >- : ClockPrecedence
infix operator >+ : ClockPrecedence

func -- ( left: ClockAngles, right: ClockAngles) -> Int {
    let minDiff = left.Minute - right.Minute
    if  left.Minute > right.Minute {
        return abs(left.Hour - right.Hour) / 60 + minDiff
    } else if left.Minute < right.Minute {
        return abs(left.Hour - right.Hour - 1) / 60 +  60 + minDiff
    } else {
        return 0
    }
}
func - ( left: ClockAngles, right: ClockAngles) -> Int {
    var h = 0
    if left.Hour > right.Hour {
        h = left.Hour - right.Hour
    } else if left.Hour < right.Hour {
        h = left.Hour - right.Hour + 24
    }
    if (left.Minute - right.Minute) > 0 {
        return h
    } else {
        return h - 1
    }
}

func >- ( left: ClockAngles, right: ClockAngles) -> Double {
    return left.HourAngle - right.HourAngle
}

func >+ ( left: ClockAngles, right: ClockAngles) -> Double {
    return left.HourAngle + right.HourAngle
}
