//
//  EnumerableExtensions.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 15/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation

extension Array {
    
    func next(by increment: Int) -> Element? {
        if self.count == 0 {
            return nil
        } else {
            let index = increment > 0 ? increment : self.count - 1 + increment
            return self[index]
        }
    }
    
}
