//
//  GraphicsExtensions.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 07/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation
import UIKit

enum LegendShapes {
    case circle
    case square
    case ellipse
}
enum SquarizeType {
    case left
    case right
}

extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat, padding: CGFloat = 0.0) {
        
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: -padding, y: -padding, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x: -padding, y: self.frame.height - thickness + padding, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x: -padding, y: -padding, width: thickness, height: self.frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x: self.frame.width - thickness + padding, y: -padding, width: thickness, height: self.frame.height)
            break
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
        self.addSublayer(border)
        
    }
    
    func addBorders(edges: [UIRectEdge], color: UIColor, thickness: CGFloat, padding: CGFloat = 0.0) {
        
        for edge in edges {
            self.addBorder(edge: edge, color: color, thickness: thickness, padding: padding)
        }

    }
}

extension CGRect {
    func squarized(to: SquarizeType) -> CGRect {
        switch to {
        case .left:
            return CGRect(origin: self.origin, size: CGSize(width: self.height, height: self.height))
        default:
            return CGRect.zero
        }
    }
    func magnified(by quotient: CGFloat) -> CGRect {
        let q = 1 - quotient
        return self.insetBy(dx: self.width*q*0.5, dy: self.height*q*0.5)
    }
    mutating func magnify(by quotient: CGFloat) {
        self = self.magnified(by: quotient)
    }
}

extension UIView {
    
    enum AnchorPoints {
        case topRight
        case bottomLeft
        case bottomRight
        case topLeft
        case center
    }
    
    func scaled(by scale: CGFloat, at anchorPoints: AnchorPoints) -> CGRect {
        switch anchorPoints {
        case .topRight:
            self.layer.anchorPoint = CGPoint(x: 1.0, y: 0.0)
        case .bottomLeft:
            self.layer.anchorPoint = CGPoint(x: 0.0, y: 1.0)
        case .bottomRight:
            self.layer.anchorPoint = CGPoint(x: 1.0, y: 1.0)
        case .topLeft:
            self.layer.anchorPoint = CGPoint(x: 0.0, y: 0.0)
        case .center:
            self.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        }
        return self.frame.applying(CGAffineTransform(scaleX: scale, y: scale))
    }
    
    func scale(by scale: CGFloat, at anchorPoints: AnchorPoints) {
        self.frame = self.scaled(by: scale, at: anchorPoints)
    }
    
    func shift(dx: CGFloat, dy: CGFloat) {
        self.frame = self.frame.offsetBy(dx: dx, dy: dy)
    }
    
    func encircle() {
        self.layer.cornerRadius = self.frame.size.width / 2
    }
}

extension UILabel {
    
    func addLegendView(withColor: UIColor, shape: LegendShapes, margin: CGFloat, scaleTo: CGFloat) {
        let legend = UIView(frame: self.frame.offsetBy(dx: self.frame.width + margin, dy: 0.0).squarized(to: .left).magnified(by: scaleTo))
        legend.backgroundColor = withColor
        switch shape {
        case .circle:
            legend.layer.cornerRadius = legend.frame.size.width / 2
        default:
            break
        }
        legend.layer.zPosition = 99
        self.superview?.addSubview(legend)
    }
    
}

extension UIColor {
    
    convenience init(hash: String, alpha: CGFloat = 1.0) {
        let hashed = hash.replacingOccurrences(of: "#", with: "")
        let segments = [hashed[0, 2], hashed[2, 4], hashed[4, 6]]
        let colors = segments.map { $0.allChars { c in c == "0" } ? 0.0 : CGFloat(Int($0, radix: 16) ?? 0) / 255 }
        self.init(red: colors[0], green: colors[1], blue: colors[2], alpha: alpha)
    }
    
    func convertToImage(size: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        self.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
}
extension UIStoryboardSegue {
    
    var contentViewController: UIViewController {
        get {
            return self.destination is UINavigationController ? (self.destination as! UINavigationController).topViewController! : self.destination
        }
    }
    
}
