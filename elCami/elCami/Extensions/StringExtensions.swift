//
//  StringExtensions.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 10/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation

extension String {
    
    subscript(begin: Int, end: Int) -> String {
        if end - begin > self.characters.count {
            return self
        } else {
            let lowerIndex = self.index(self.startIndex, offsetBy: begin)
            let upperIndex = self.index(lowerIndex, offsetBy: end-begin)
            return self.substring(with: Range<String.Index>(uncheckedBounds: (lower: lowerIndex, upper: upperIndex)))
        }
    }
    
    func allChars(body: (Character) -> Bool) -> Bool {
        
        for el in self.characters {
            if !body(el) {
                return false;
            }
        }
        return true;
        
    }
    func index(of string: String, options: String.CompareOptions = .literal) -> String.Index? {
        return range(of: string, options: options, range: nil, locale: nil)?.lowerBound
    }
    func indexes(of string: String, options: String.CompareOptions = .literal) -> [String.Index] {
        var result: [String.Index] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex, locale: nil) {
            result.append(range.lowerBound)
            start = range.upperBound
        }
        return result
    }
    func ranges(of string: String, options: String.CompareOptions = .literal) -> [Range<String.Index>] {
        var result: [Range<String.Index>] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex, locale: nil) {
            result.append(range)
            start = range.upperBound
        }
        return result
    }
}
