//
//  CoreDataProviderDelegate.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 14/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation
import UIKit

protocol DataProviderDelegate {
    associatedtype TOwner
    associatedtype Element
    
    var owner: TOwner? { get set }
    var data: [Element]? { get }
    var filtered: [Element]? { get }
    
}


extension DataProviderDelegate where Self: UITableViewController {
    
    func parseActions(
        config: [(String, UIColor, Bool, (Element) -> Bool, (Element) -> Void)],
        at indexPath: IndexPath) -> [UITableViewRowAction] {
        
        guard let app = UIApplication.shared.delegate as? AppDelegate else {
            return []
        }
        
        var actions = [UITableViewRowAction]()
        for (title, color, enabled, filter, committer) in config {
            let action = UITableViewRowAction(style: .normal,
                                              title: title,
                                              handler: { (action: UITableViewRowAction, index: IndexPath) in
                                                if let country = self.filtered?.filter({ filter($0) })[index.row] {
                                                    committer(country)
                                                    app.saveContext()
                                                    self.tableView.reloadData()
                                                }
            })
            if enabled {
                action.backgroundColor = color
                actions.append(action)
            }
        }
        return actions
    }
    
}
