//
//  DistrictViewCell.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 15/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import UIKit
import CoreData

class DistrictViewCell: UICollectionViewCell {

    @IBOutlet weak var DistrictLabel: UILabel!
    @IBOutlet weak var AddSwitch: UISwitch!
    @IBOutlet weak var MainView: UIView!
    
    private var coreCity: CoreCity!
    var collectionViewController: UICollectionViewController!
    
    func configureSwitch<T: DataProviderDelegate>(_ info: (controller: T, at: IndexPath)) {
        self.collectionViewController = info.controller as! UICollectionViewController
        self.coreCity = info.controller.filtered?[info.at.item] as? CoreCity
        self.AddSwitch.setOn(self.coreCity.added == true, animated: true)
    }
    
    @IBAction func addSwitchToggled(_ sender: UISwitch) {
        
        guard let app = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        if sender.isOn {
            if !self.coreCity.added {
                app.parseIdentity(callback: { (identity) in
                    self.coreCity.added = true
                    identity.addToCities(self.coreCity)
                    app.saveContext()
                    self.DistrictLabel.textColor = UIColor(hash: "#008080")
                    //self.collectionViewController.collectionView?.reloadData()
                })
            }
        } else {
            if self.coreCity.added {
                app.parseIdentity(callback: { (identity) in
                    self.coreCity.added = false
                    identity.removeFromCities(self.coreCity)
                    app.saveContext()
                    self.DistrictLabel.textColor = UIColor(hash: "#238553")
                    //self.collectionViewController.collectionView?.reloadData()
                })
            }
        }
    }
    
}
