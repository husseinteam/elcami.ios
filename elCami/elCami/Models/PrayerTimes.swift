//
//  PrayerTimes.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 06/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation
import UIKit
import CoreData

import SwiftyJSON

struct PrayerTimes {
    let Fajr: PrayerTime
    let Sun: PrayerTime
    let Zuhr: PrayerTime
    let Asr: PrayerTime
    let Maghhrib: PrayerTime
    let Isha: PrayerTime
    let NextFajr: PrayerTime
    let MoonSrc: String
    let HicriTarih: String
    let Enlem: Int
    let Boylam: Int
    let KibleAcisi: String
    let UlkeAdi: String
    let SehirAdi: String
    let Qibla: PrayerTime
    let Sunset: PrayerTime
    let Sunrise: PrayerTime
    let ItemId: Int
    
    init(json: JSON) {
        self.NextFajr = PrayerTime(name: PrayerNames.NextFajr, clock: json["NextImsak"].stringValue, color: UIColor(hash: "#007C00"))
        self.Isha = PrayerTime(name: PrayerNames.Isha, clock: json["Yatsi"].stringValue, color: UIColor(hash: "#FF0000"), nextPrayerTime: self.NextFajr)
        self.Maghhrib = PrayerTime(name: PrayerNames.Maghhrib, clock: json["Aksam"].stringValue, color: UIColor(hash: "#FF8000"), nextPrayerTime: self.Isha)
        self.Asr = PrayerTime(name: PrayerNames.Asr, clock: json["Ikindi"].stringValue, color: UIColor(hash: "#008080"), nextPrayerTime: self.Maghhrib)
        self.Zuhr = PrayerTime(name: PrayerNames.Zuhr, clock: json["Ogle"].stringValue, color: UIColor(hash: "#0000FF"), nextPrayerTime: self.Asr)
        self.Sun = PrayerTime(name: PrayerNames.Sun, clock: json["Gunes"].stringValue, color: UIColor(hash: "#FFB100"), nextPrayerTime: self.Zuhr)
        self.Fajr = PrayerTime(name: PrayerNames.Fajr, clock: json["Imsak"].stringValue, color: UIColor(hash: "#FF21FF"), nextPrayerTime: self.Sun)
        self.MoonSrc = json["MoonSrc"].stringValue
        self.HicriTarih = json["HicriTarih"].stringValue
        self.Enlem = json["Enlem"].intValue
        self.Boylam = json["Boylam"].intValue
        self.KibleAcisi = json["KibleAcisi"].stringValue
        self.UlkeAdi = json["UlkeAdi"].stringValue
        self.SehirAdi = json["SehirAdi"].stringValue
        self.Qibla = PrayerTime(name: PrayerNames.Qibla, clock: json["KibleSaati"].stringValue, color: UIColor.blue)
        self.Sunset = PrayerTime(name: PrayerNames.Sunset, clock: json["GunesBatis"].stringValue, color: UIColor.orange)
        self.Sunrise = PrayerTime(name: PrayerNames.Sunrise, clock: json["GunesDogus"].stringValue, color: UIColor.orange)
        self.ItemId = json["ItemId"].intValue
    }
    
    var arrivingPrayer: PrayerTime {
        get {
            let arriving = [self.Fajr, self.Sun, self.Zuhr, self.Asr, self.Maghhrib, self.Isha]
            return arriving.filter { $0.ExactTime > Date() }.first ?? self.NextFajr
        }
    }

    func resultify() -> [CoreCity] {
        let request: NSFetchRequest<CoreCity> = CoreCity.fetchRequest()
        do {
            let searchResults = try request.execute()
            return searchResults
        } catch {
            fatalError("Error with request: \(error)")
        }
    }
    
}

