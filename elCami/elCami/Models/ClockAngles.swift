//
//  ClockAngles.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 07/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation

struct ClockAngles {
    
    let Hour: Int
    let Minute: Int
    let Second: Int
    let HourAngle: Double
    let MinuteAngle: Double
    let SecondAngle: Double
    
    init(hour: Int, minute: Int, second: Int) {
        let fractionalHours = Double(hour) + Double(minute) / 60.0
        self.Hour = hour
        self.Minute = minute
        self.Second = second
        self.HourAngle = .pi * 2.0 * fractionalHours / 24.0 + .pi
        self.MinuteAngle = .pi * 2.0 * Double(minute) / 60.0 + .pi
        self.SecondAngle = .pi * 2.0 * Double(second) / 60.0 + .pi
    }
    
    init(prayerTime: PrayerTime?) {
        if let time = prayerTime {
            self.init(hour: time.Hour, minute: time.Minute, second: time.Second)
        } else {
            self.init()
        }
    }
    
    init() {
        self.init(hour: 0, minute: 0, second: 0)
    }
    
    init(date: Date) {
        let timeComponents = Calendar.current.dateComponents([.hour, .minute, .second], from: date)
        let hour = timeComponents.hour! % 24
        let minute = timeComponents.minute!
        let second = timeComponents.second!
        self.init(hour: hour, minute: minute, second: second)
    }
    
}
