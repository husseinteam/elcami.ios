//
//  Hadith.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 10/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation

class Hadith {
    
    private let transcript: String
    let Source: String
    
    var Transcript: String? {
        get {
            return "\(self.transcript)\n\(self.Source)"
        }
    }
    static let Prophet = "Hz. Muhammed Mustafa Sallallahü Aleyhi Vesellem"
    
    init(transcript: String?, source: String? = nil) {
        self.transcript = transcript ?? ""
        self.Source = source ?? Hadith.Prophet
    }
    
}
