//
//  State.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 06/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation
import UIKit
import CoreData

struct State {
    
    var Disabled: Bool? = nil
    var Group: String? = nil
    var Selected: Bool? = nil
    let Name: String
    let Code: String
    
    init(name: String?, code: String?) {
        self.Name = name ?? ""
        self.Code = code ?? ""
    }
 
    static func seed(byCountry: CoreCountry,
                     collect: @escaping (([CoreState]) -> Void),
                     failback: ((FetchPostEngine, FetchPostType, Error) -> Void)? = nil) {
        State.countify(countryCode: byCountry.code, callback: { count in
            if count == 0 {
                let pte = PrayerTimesEngine()
                if let fb = failback {
                    pte.subscribeToFetchPostEngine(failback: fb)
                }
                pte.getStates(by: Country(name: byCountry.name, code: byCountry.code))
                { states in
                    var coreStates = [CoreState]()
                    for state in states {
                        if let coreState = state.store(owner: byCountry) {
                            coreStates.append(coreState)
                        }
                    }
                    collect(coreStates)
                }
                
            } else {
                State.resultify(countryCode: byCountry.code, callback: { (results) in
                    collect(results)
                })
            }
        })
    }
    
    static func resultify(countryCode: String?, callback: @escaping ([CoreState]) -> Void) {
        guard let app = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let moc = app.persistentContainer.viewContext
        let request: NSFetchRequest<CoreState> = CoreState.fetchRequest()
        moc.perform {
            do {
                request.predicate =
                    NSPredicate(format: "country.code == %@", countryCode ?? "")
                let searchResults = try request.execute()
                callback(searchResults)
            } catch {
                fatalError("Error with request: \(error)")
            }
        }
    }
    
    static func countify(countryCode: String?, callback: @escaping (Int) -> Void) {
        guard let app = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let moc = app.persistentContainer.viewContext
        
        let request: NSFetchRequest<CoreState> = CoreState.fetchRequest()
        request.predicate =
            NSPredicate(format: "country.code == %@", countryCode ?? "")
        do {
            let count = try moc.count(for: request)
            callback(count)
        } catch {
            fatalError("Error with request: \(error)")
        }
    }
    
    func store(owner: CoreCountry, favorite: Bool = false, cities: [City]? = nil) -> CoreState? {
        guard let app = UIApplication.shared.delegate as? AppDelegate else {
            return nil
        }
        let ctx = app.persistentContainer.viewContext
        let state = CoreState(context: ctx)
        state.name = self.Name
        state.code = self.Code
        state.favorite = favorite
        state.country = owner
        if let citili = cities {
            for city in citili {
                let coreCity = CoreCity(context: ctx)
                coreCity.state = state
                coreCity.name = city.Name
                coreCity.code = city.Code
                coreCity.favorite = false
                state.addToCities(coreCity)
            }
        }
        do {
            try ctx.save()
        } catch {
            fatalError("Error with request: \(error)")
        }
        return state
    }
    
}
