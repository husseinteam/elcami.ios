//
//  City.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 06/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation
import UIKit
import CoreData

struct City {
    
    var Disabled: Bool? = nil
    var Group: String? = nil
    var Selected: Bool? = nil
    let Name: String
    let Code: String
    
    init(name: String?, code: String?) {
        self.Name = name ?? ""
        self.Code = code ?? ""
    }
    
    static func seed(byState: CoreState, collect: @escaping (([CoreCity]) -> Void), failback: ((FetchPostEngine, FetchPostType, Error) -> Void)? = nil) {
        City.countify(stateCode: byState.code, callback: { count in
            if count == 0 {
                let pte = PrayerTimesEngine()
                if let fb = failback {
                    pte.subscribeToFetchPostEngine(failback: fb)
                }
                pte.getCities(by: State(name: byState.name, code: byState.code)) { cities in
                    var coreCities = [CoreCity]()
                    for city in cities {
                        if let coreCity = city.store(owner: byState) {
                            coreCities.append(coreCity)
                        }
                    }
                    collect(coreCities)
                }
                
            } else {
                City.resultify(stateCode: byState.code, callback: { (results) in
                    collect(results)
                })
            }
        })
    }
    
    static func resultify(stateCode: String?, callback: @escaping ([CoreCity]) -> Void) {
        guard let app = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let moc = app.persistentContainer.viewContext
        let request: NSFetchRequest<CoreCity> = CoreCity.fetchRequest()
        moc.perform {
            do {
                request.predicate =
                    NSPredicate(format: "state.code == %@", stateCode ?? "")
                let searchResults = try request.execute()
                callback(searchResults)
            } catch {
                fatalError("Error with request: \(error)")
            }
        }
    }
    
    static func countify(stateCode: String?, callback: @escaping (Int) -> Void) {
        guard let app = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let moc = app.persistentContainer.viewContext
        
        let request: NSFetchRequest<CoreCity> = CoreCity.fetchRequest()
        request.predicate =
            NSPredicate(format: "state.code == %@", stateCode ?? "")
        do {
            let count = try moc.count(for: request)
            callback(count)
        } catch {
            fatalError("Error with request: \(error)")
        }
    }
    
    func store(owner: CoreState, favorite: Bool = false, date: NSDate = NSDate(), prayerTimesList: [PrayerTimes]? = nil) -> CoreCity? {
        guard let app = UIApplication.shared.delegate as? AppDelegate else {
            return nil
        }
        let ctx = app.persistentContainer.viewContext
        let city = CoreCity(context: ctx)
        city.name = self.Name
        city.code = self.Code
        city.favorite = favorite
        city.state = owner
        if let prayerTimesLi = prayerTimesList {
            for prayerTimes in prayerTimesLi {
                let corePrayerTimes = CorePrayerTimes(context: ctx)
                for time in [prayerTimes.Fajr,
                             prayerTimes.Sun,
                             prayerTimes.Zuhr,
                             prayerTimes.Asr,
                             prayerTimes.Maghhrib,
                             prayerTimes.Isha,
                             prayerTimes.NextFajr,
                             prayerTimes.Qibla,
                             prayerTimes.Sunset,
                             prayerTimes.Sunrise
                    ] {
                        let corePrayerTime = CorePrayerTime(context: ctx)
                        corePrayerTime.hour = Int64(time.Hour)
                        corePrayerTime.minute = Int64(time.Minute)
                        corePrayerTime.second = Int64(time.Second)
                        corePrayerTimes.addToCurrentPrayerTimes(corePrayerTime)
                }
                corePrayerTimes.city = city
                corePrayerTimes.date = date
                city.addToCurrentPrayerTimes(corePrayerTimes)
            }
        }
        
        do {
            try ctx.save()
        } catch {
            fatalError("Error with request: \(error)")
        }
        return city
    }
    
}
