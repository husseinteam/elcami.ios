//
//  Country.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 06/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation
import UIKit
import CoreData

struct Country {
    
    let Name: String
    let Code: String
    
    init(name: String?, code: String?) {
        self.Name = name ?? ""
        self.Code = code ?? ""
    }
    
    static func seed(collect: @escaping (([CoreCountry]) -> Void), failback: ((FetchPostEngine, FetchPostType, Error) -> Void)? = nil) {
        Country.countify(callback: { count in
            if count == 0 {
                let pte = PrayerTimesEngine()
                if let fb = failback {
                    pte.subscribeToFetchPostEngine(failback: fb)
                }
                let countries = pte.getCountries()
                var coreCountries = [CoreCountry]()
                for country in countries {
                    if let coreCountry = country.store(
                        favorite: country.Name == "TÜRKİYE") {
                        coreCountries.append(coreCountry)
                    }
                }
                collect(coreCountries)
            } else {
                Country.resultify(callback: { (results) in
                    collect(results)
                })
            }
        })
    }
    
    static func resultify(callback: @escaping ([CoreCountry]) -> Void) {
        guard let app = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let moc = app.persistentContainer.viewContext
        let request: NSFetchRequest<CoreCountry> = CoreCountry.fetchRequest()
        moc.perform {
            do {
                let searchResults = try request.execute()
                callback(searchResults)
            } catch {
                fatalError("Error with request: \(error)")
            }
        }
    }
    
    static func countify(callback: @escaping (Int) -> Void) {
        guard let app = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let moc = app.persistentContainer.viewContext
        
        let request: NSFetchRequest<CoreCountry> = CoreCountry.fetchRequest()
        do {
            let count = try moc.count(for: request)
            callback(count)
        } catch {
            fatalError("Error with request: \(error)")
        }
    }
    
    func store(favorite: Bool = false, states: [State]? = nil) -> CoreCountry? {
        guard let app = UIApplication.shared.delegate as? AppDelegate else {
            return nil
        }
        let ctx = app.persistentContainer.viewContext
        let country = CoreCountry(context: ctx)
        country.name = self.Name
        country.code = self.Code
        country.favorite = favorite
        if let stateli = states {
            for state in stateli {
                let coreState = CoreState(context: ctx)
                coreState.country = country
                coreState.name = state.Name
                coreState.code = state.Code
                coreState.favorite = false
                country.addToStates(coreState)
            }
        }
        do {
            try ctx.save()
        } catch {
            fatalError("Error with request: \(error)")
        }
        return country
    }
    
}
