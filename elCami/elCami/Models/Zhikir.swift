//
//  Zhikir.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 11/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation

struct Zhikir {
    
    let Title: String
    let Transcript: String
    let Count: Int
    
    init(_ title: String, transcript: String? = nil, count: Int = 1) {
        self.Transcript = transcript ?? title
        self.Count = count
        self.Title = title
    }
    
}
