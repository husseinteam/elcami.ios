//
//  PrayerTime.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 07/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation
import UIKit

class PrayerTime  {
    
    let Name: PrayerNames
    
    let Hour: Int
    let Minute: Int
    let Second: Int
    
    let BaseInterval: TimeInterval
    let ExactTime: Date
    
    let DefinedColor: UIColor
    let NextPrayerTime: PrayerTime?
    
    init(name: PrayerNames, clock: String, color: UIColor, nextPrayerTime: PrayerTime? = nil) {
        self.Name = name
        self.DefinedColor = color
        self.NextPrayerTime = nextPrayerTime
        let parts = clock.components(separatedBy: ":")
        self.BaseInterval = TimeInterval(Int(parts[1])! * 60 + Int(parts[0])! * 3600)
        self.Hour = Int(parts[0])!
        self.Minute = Int(parts[1])!
        self.Second = 0
        self.ExactTime = Date(timeInterval: self.BaseInterval, since: Calendar.current.startOfDay(for: Date()))
    }
    
    var clockHour: String {
        get {
            return self.Hour.digitalized()
        }
    }
    var clockMinute: String {
        get {
            return self.Minute.digitalized()
        }
    }
    var clock: String {
        get {
            return "\(self.clockHour):\(self.clockMinute)"
        }
    }
    
}

enum PrayerNames {
    case Fajr
    case Sun
    case Zuhr
    case Asr
    case Maghhrib
    case Isha
    case Sunrise
    case Sunset
    case NextFajr
    case Qibla
    
    var description: String {
        get {
            var repr: String;
            switch self {
                case .Fajr: repr = "imsak"
                case .Zuhr: repr = "öğle"
                case .Asr: repr = "ikindi"
                case .Maghhrib: repr = "akşam"
                case .Isha: repr = "yatsı"
                case .Sun: repr = "güneş"
                case .Sunrise: repr = "gün doğuşu"
                case .Sunset: repr = "gün batımı"
                case .NextFajr: repr = "imsak"
                case .Qibla: repr = "kıble saati"
            }
            return repr.uppercased(with: Locale(identifier: "en"))
        }
    }
    
}
