//
//  State.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 06/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import Foundation
import UIKit
import CoreData

struct Identity {
    
    var UUID: String?
    
    init(uuid: String? = nil) {
        self.UUID = uuid ?? UIDevice.current.identifierForVendor?.uuidString
    }
 
    static func resultify(uuid: String?, callback: @escaping ([CoreIdentity]) -> Void) {
        guard let app = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let moc = app.persistentContainer.viewContext
        let request: NSFetchRequest<CoreIdentity> = CoreIdentity.fetchRequest()
        moc.perform {
            do {
                request.predicate =
                    NSPredicate(format: "uuid == %@", uuid ?? "")
                let searchResults = try request.execute()
                callback(searchResults)
            } catch {
                fatalError("Error with request: \(error)")
            }
        }
    }
    
    static func countify(uuid: String?, callback: @escaping (Int) -> Void) {
        guard let app = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let moc = app.persistentContainer.viewContext
        
        let request: NSFetchRequest<CoreIdentity> = CoreIdentity.fetchRequest()
        request.predicate =
            NSPredicate(format: "uuid == %@", uuid ?? "")
        do {
            let count = try moc.count(for: request)
            callback(count)
        } catch {
            fatalError("Error with request: \(error)")
        }
    }
    
    func store() -> CoreIdentity? {
        guard let app = UIApplication.shared.delegate as? AppDelegate else {
            return nil
        }
        let ctx = app.persistentContainer.viewContext
        let identity = CoreIdentity(context: ctx)
        identity.uuid = self.UUID
        do {
            try ctx.save()
        } catch {
            fatalError("Error with request: \(error)")
        }
        return identity
    }
    
}
