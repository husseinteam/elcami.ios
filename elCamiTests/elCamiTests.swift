//
//  iSSalatTests.swift
//  iSSalatTests
//
//  Created by Hüseyin Sönmez on 03/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import XCTest
import SwiftyJSON

@testable import iSSalat

class iSSalatTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testThreeDropdowns() {
        let scraper = ScraperEngine(site: URL(string: "http://www.diyanet.gov.tr/tr/namazvakitleri"))
        scraper.process { (doc) in
            for country in doc.css("#Country option"){
                XCTAssert(country.content != nil)
            }
            for state in doc.css("#State option") {
                XCTAssert(state.content != nil)
            }
            for city in doc.css("#City option") {
                XCTAssert(city.content != nil)
            }
        }
    }
    func testState() {
        let scraper = ScraperEngine(site: URL(string: "http://www.diyanet.gov.tr/tr/namazvakitleri"))
        let expectation = self.expectation(description: "selectsState")
        scraper.process { (doc) in
            let turkiye = doc.css("#Country option").filter { $0.content! == "TÜRKİYE" }.first
            XCTAssertTrue(turkiye != nil)
            let fetcher = FetchPostEngine()
            let countrycode = turkiye!.attributes()["value"]!
            fetcher.fetchJSON(from: "http://www.diyanet.gov.tr/PrayerTime/FillState?countryCode=\(countrycode)") { json in
                for (_,state):(String, JSON) in json {
                    XCTAssertTrue(state["Text"].string != nil)
                    XCTAssertTrue(state["Value"].string != nil)
                }
                expectation.fulfill()
            }
            self.waitForExpectations(timeout: 10.0, handler: { (err) in
                XCTAssertNil(err, err.debugDescription)
            })
        }
        
    }
    func testImsakiye() {
        let scraper = ScraperEngine(site: URL(string: "http://www.diyanet.gov.tr/tr/namazvakitleri"))
        let expectation = self.expectation(description: "selectsCity")
        scraper.process { (doc) in
            let turkiye = doc.css("#Country option").filter { $0.content! == "TÜRKİYE" }.first
            XCTAssertTrue(turkiye != nil)
            let fetcher = FetchPostEngine()
            let countrycode = turkiye!.attributes()["value"]!
            fetcher.fetchJSON(from: "http://www.diyanet.gov.tr/PrayerTime/FillState?countryCode=\(countrycode)") { json in
                let bolu = json.filter { $0.1["Text"] == "BOLU" }.first
                XCTAssert(bolu != nil)
                fetcher.postJSON(to: "http://www.diyanet.gov.tr/PrayerTime/PrayerTimesSet", parameters: ["countryName":"2","stateName":"518","name":"9318"]) { response in
                    XCTAssert((response["Imsak"].null == nil))
                    expectation.fulfill()
                }
            }
            self.waitForExpectations(timeout: 10.0, handler: { (err) in
                XCTAssertNil(err, err.debugDescription)
            })
        }
    }
    
    //http://www.diyanet.gov.tr/PrayerTime/FillCity?itemId=518
    
    func testXMLElementAttributeExtension() {
        let scraper = ScraperEngine(site: URL(string: "http://www.diyanet.gov.tr/tr/namazvakitleri"))
        scraper.process { (doc) in
            let turkiye = doc.css("#Country option").filter { $0.content! == "TÜRKİYE" }.first
            XCTAssertTrue(turkiye!.attributes().count > 0)
        }
    }
    
}
