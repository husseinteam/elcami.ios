//
//  PrayerTimesEngineTests.swift
//  iSSalat
//
//  Created by Hüseyin Sönmez on 06/12/2016.
//  Copyright © 2016 hussein.ist. All rights reserved.
//

import XCTest
import SwiftyJSON

@testable import iSSalat

class PrayerTimesEngineTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCountries() {
        let pte = PrayerTimesEngine()
        let countries = pte.getCountries()
        XCTAssert(countries.count > 0)
    }
    
    func testStates() {
        let expectation = self.expectation(description: "states populated")
        let pte = PrayerTimesEngine()
        let turkiye = pte.getCountries().filter { $0.Code == "2" }.first!
        pte.getStates(by: turkiye) { states in
            XCTAssert(states.count > 0)
            expectation.fulfill()
        }
        self.waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testCities() {
        let expectation = self.expectation(description: "cities populated")
        let pte = PrayerTimesEngine()
        let turkiye = pte.getCountries().filter { $0.Code == "2" }.first!
        pte.getStates(by: turkiye) { cities in
            let bolu = cities.filter { $0.Code == "518" }.first!
            pte.getCities(by: bolu) { cities in
                XCTAssert(cities.count > 0)
                XCTAssert(cities.filter { $0.Code == "9318" }.first != nil)
                expectation.fulfill()
            }
        }
        self.waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testPrayerTimes() {
        let expectation = self.expectation(description: "prayer times fetched")
        let pte = PrayerTimesEngine()
        let turkiye = pte.getCountries().filter { $0.Code == "2" }.first!
        pte.getStates(by: turkiye) { states in
            let bolu = states.filter { $0.Code == "518" }.first!
            pte.getCities(by: bolu) { cities in
                XCTAssert(cities.count > 0)
                XCTAssert(cities.filter { $0.Code == "9318" }.first != nil)
                let merkez = cities.filter { $0.Code == "9318" }.first!
                pte.getPrayerTimes(by: turkiye, state: bolu, city: merkez, callback: { times in
                    XCTAssert(times?.Fajr != nil)
                    debugPrint(times!.Fajr)
                    expectation.fulfill()
                })
            }
        }
        self.waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testPrayerTimesByCode() {
        let expectation = self.expectation(description: "prayer times fetched")
        let pte = PrayerTimesEngine()
        pte.getPrayerTimes(by: "2", stateCode: "518", cityCode: "9318") { times in
            XCTAssert(times?.Fajr != nil)
            debugPrint(times!.Fajr)
            expectation.fulfill()
        }
        self.waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
